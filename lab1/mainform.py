from template import Ui_MainWindow, QtWidgets
from modul import Modul

class MainForm(Ui_MainWindow):
    def __init__(self):
        self.modul = Modul()

    def retranslateUi(self, MainWindow):
        Ui_MainWindow.retranslateUi(self, MainWindow)
        self.pushButton_calc.clicked.connect(self.slotCalculation)

    def slotCalculation(self):
        #getValues
        tA = str(self.lineEdit_ta.text())
        tB = str(self.lineEdit_tb.text())
        tC = str(self.lineEdit_tc.text())
        tD = str(self.lineEdit_td.text())
        tE = str(self.lineEdit_te.text())
        tA = self.modul.strToIntElements(tA.split(' '))
        tB = self.modul.strToIntElements(tB.split(' '))
        tC = self.modul.strToIntElements(tC.split(' '))
        tD = self.modul.strToIntElements(tD.split(' '))
        tE = self.modul.strToIntElements(tE.split(' '))
        r = int(self.spinBox_reserv.value())
        data = self.getDataOfTableWidget()

        #calc
        opts = self.modul.getOptions(tA, tB, tC, tD, tE)
        chances = self.modul.getChances(opts)
        dT = self.modul.calcDataTable(data, chances, r)

        #out
        self.constructTableOptions(opts)
        self.constructTableChances(chances)
        string = self.modul.printMaxLikelihood(dT)
        string += '\n'
        string += self.modul.printPrincipleBayes(dT)
        string += '\n'
        string += self.modul.printGuaranteedAssessment(dT)
        self.textBrowser_conclusions.setText(string)

    def getDataOfTableWidget(self):
        data = [[], []]
        for i in range(0, int(self.tableWidget_research.columnCount())):
            data[0].append(int(self.tableWidget_research.item(0,i).text()))
            data[1].append(int(self.tableWidget_research.item(1,i).text()))
        return data

    def constructTableChances(self, chances):
        amt = len(chances[0])
        self.tableWidget_chances.setColumnCount(amt)
        for i in range(0, amt):
            for j in range(0, 2):
                item = QtWidgets.QTableWidgetItem()
                item.setText(str(chances[j][i]))
                self.tableWidget_chances.setItem(j, i, item)

    def constructTableOptions(self, opts):
        self.tableWidget_options.setRowCount(opts['amt'])
        for i in range(0, opts['amt']):
            for j in range(0, 5):
                item = QtWidgets.QTableWidgetItem()
                item.setText(str(opts['opts'][j][i]))
                self.tableWidget_options.setItem(i, j, item)