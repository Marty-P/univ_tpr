import sys
from mainform import MainForm
from PyQt5 import QtCore, QtGui, QtWidgets

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = MainForm()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
