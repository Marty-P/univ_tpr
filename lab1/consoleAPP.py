import modul as k
modul = k.Modul()
'''
print("Работы:")
tA = input('tA: ')
tB = input('tB: ')
tC = input('tC: ')
tD = input('tD: ')
tE = input('tE: ')
tA = modul.strToIntElements(tA.split(' '))
tB = modul.strToIntElements(tB.split(' '))
tC = modul.strToIntElements(tC.split(' '))
tD = modul.strToIntElements(tD.split(' '))
tE = modul.strToIntElements(tE.split(' '))

r = int(input('Резерв: '))

print('Маркетинговое исследование:')
n = int(input('Кол-во пунктов: '))
data = [[], []]
for i in range(0, n):
    string = input(str(i + 1) + ') ')
    temp = string.split(' ')
    data[0].append(int(temp[0]))
    data[1].append(int(temp[1]))
'''

tA = [2]
tB = [1]
tC = [2, 3, 4]
tD = [2, 3, 4]
tE = [2, 3, 4]
r = 20
data = [[3, 4, 5, 6, 7],
        [120, 110, 100, 50, 0]]

opts = modul.getOptions(tA, tB, tC, tD, tE)
chances = modul.getChances(opts)
dT = modul.calcDataTable(data, chances, r)
modul.printTable(dT)
print(modul.printMaxLikelihood(dT))
print(modul.printPrincipleBayes(dT))
print(modul.printGuaranteedAssessment(dT))