#Подключаемый модуль для первой лабораторной по ТРП
#Author: Martynov Pavel

import copy
import fractions
import prettytable

class Modul:
    # Конвертация элеметов списка из str в int
    def strToIntElements(self, x):
        return [int(i) for i in x]


    # поиск элемента в списке
    #
    # возвращает номер первого входящего элемента если существует
    # и -1 если элемента нет
    def getIndexElement(self, x, y):
        amt = len(x)
        for i in range(0, amt):
            if x[i] == y:
                return i
        return -1


    # получение вариантов
    def getOptions(self, tA, tB, tC, tD, tE):
        a = copy.copy(tA)
        b = copy.copy(tB)
        c = copy.copy(tC)
        d = copy.copy(tD)
        e = copy.copy(tE)
        amt = 0
        opts = [[], [], [], [], []]
        while a != []:
            while b != []:
                while c != []:
                    while d != []:
                        while e != []:
                            opts[0].append(a[0])
                            opts[1].append(b[0])
                            opts[2].append(c[0])
                            opts[3].append(d[0])
                            opts[4].append(e[0])
                            amt += 1
                            e.pop(0)
                        d.pop(0)
                        e = copy.copy(tE)
                    c.pop(0)
                    d = copy.copy(tD)
                    e = copy.copy(tE)
                b.pop(0)
                c = copy.copy(tE)
                d = copy.copy(tD)
                e = copy.copy(tE)
            a.pop(0)
            b = copy.copy(tD)
            c = copy.copy(tE)
            d = copy.copy(tD)
            e = copy.copy(tE)
        return {'amt': amt, 'opts': opts}


    # нахождение максивального срока
    def getMaxTime(self, tA, tB, tC, tD, tE):
        return max(tA + tD, tC, tB + tE)


    # получение списка максимальных сроков из вариантов
    def getListMaxTime(self, amt, opts):
        listMaxTime = []
        for i in range(0, amt):
            listMaxTime.append(self.getMaxTime(opts[0][i], opts[1][i],
                                          opts[2][i], opts[3][i],
                                          opts[4][i]))
        return listMaxTime


    # получение словаря
    def getDictionary(self, listMaxTime):
        dictionary = [[], []]
        while listMaxTime != []:
            dictionary[0].append(listMaxTime.pop(0))
            dictionary[1].append(1)
            flag = 1
            while flag:
                index = self.getIndexElement(listMaxTime,
                                        dictionary[0][-1])
                if index != -1:
                    listMaxTime.pop(index)
                    dictionary[1][-1] += 1
                else:
                    flag = 0
        return dictionary


    # получение вероятности
    def getChances(self, opts):
        listMaxTime = self.getListMaxTime(opts['amt'], opts['opts'])
        dictionary = self.getDictionary(listMaxTime)
        lenDictionary = len(dictionary[0])
        chances = [dictionary[0], []]

        for i in range(0, lenDictionary):
            chances[1].append(fractions.Fraction(dictionary[1][i], opts['amt']))
        return chances


    # печать теблицы
    def printTable(self, data):
        dataT = copy.deepcopy(data)
        dataT[0].insert(0, 'x\\t')
        dataT[1][0].insert(0, 0)
        dataT[1][1].insert(0, 1)
        table = prettytable.PrettyTable()
        table.field_names = dataT[0]
        table.add_row(dataT[1][0])
        table.add_row(dataT[1][1])
        print(table)


    # вычисление данных для таблицы
    # выходные данные список (кол-во лет/прибыль/вероятность)
    def calcDataTable(self, data, chances, r):
        dataTable = [[], [[], []], []]
        amt = len(chances[0])
        for i in range(0, amt):
            index = self.getIndexElement(data[0], chances[0][i])
            if index != -1:
                dataTable[0].append(data[0][index])
                dataTable[1][0].append(data[1][index])
                if index != 0:
                    dataTable[1][1].append(data[1][index - 1] - r)
                else:
                    dataTable[1][1].append(None)
                dataTable[2].append(chances[1][i])
        return dataTable


    # принцип максимальной правдоподобности
    def printMaxLikelihood(self, data):
        string = 'Принцип максимального правдоподобия:\n'
        index = self.getIndexMaxElement(data[2])
        string = string + 'F(0,' + str(data[0][index]) + ') = ' + str(data[1][0][index]) + '\n'
        string = string + 'F(1,' + str(data[0][index]) + ') = ' + str(data[1][1][index]) + '\n'
        if data[1][0][index] < data[1][1][index]:
            string += 'Использование резерва: целесообразно\n'
        else:
            string += 'Использование резерва: НЕ целесообразно\n'
        return string


    # получыение индекса максимального элемента
    def getIndexMaxElement(self, listEl):
        index = 0
        for i in range(1, len(listEl)):
            if listEl[index] < listEl[i]:
                index = i
        return index


    # получыение индекса максимального элемента
    def getIndexMinElement(self, listEl):
        index = 0
        for i in range(1, len(listEl)):
            if listEl[index] > listEl[i]:
                index = i
        return index

    # принцип Байеса
    def printPrincipleBayes(self, data):
        string = 'Принцип Байеса:\n'
        mf0 = 0
        mf1 = 0
        for i in range(0, len(data[0])):
            mf0 += data[1][0][i] * data[2][i]
            mf1 += data[1][1][i] * data[2][i]
        string = string + 'MF(0, T): ' + str(mf0) + '\n'
        string = string + 'MF(1, T): ' + str(mf1) + '\n'
        if mf0 < mf1:
            string += 'Использование резерва: целесообразно\n'
        else:
            string += 'Использование резерва: НЕ целесообразно\n'
        return string


    # принцип гарантированных оценок
    def printGuaranteedAssessment(self, data):
        string = 'Принцип гарантированных оценок:\n'
        index = self.getIndexMinElement(data[1][0])
        string = string + 'F(0,' + str(data[0][index]) + ') = ' + str(data[1][0][index]) + '\n'
        string = string + 'F(1,' + str(data[0][index]) + ') = ' + str(data[1][1][index]) + '\n'
        if data[1][0][index] < data[1][1][index]:
            string += 'Использование резерва: целесообразно\n'
        else:
            string += 'Использование резерва: НЕ целесообразно\n'
        return string